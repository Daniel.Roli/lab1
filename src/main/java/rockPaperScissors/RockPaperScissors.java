package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;   

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
    	Random random = new Random();
    	while(true){
    		String comcho = rpsChoices.get(random.nextInt(3));
    		System.out.println("Let's play round "+roundCounter);
    		String placho = validate("Your choice (Rock/Paper/Scissors)?");
    		String winner = checkwinner(comcho, placho);
    		if(winner.equals("draw")){
    			System.out.println("Human chose "+placho+", computer chose "+comcho+". It's a tie!");
    		}else {
    			System.out.println("Human chose "+placho+", computer chose "+comcho+". "+winner+ " wins!");
    		}
    		System.out.println("Score: human "+humanScore+", computer "+computerScore);
    		String Continue = readInput("Do you wish to continue playing? (y/n)?");
    		while(true){
    			if(!Continue.equals("n") && !Continue.equals("y")) {
    				Continue = readInput("Do you wish to continue playing? (y/n)?");
    			}else {
    				break;
    			}
    		}
    		if(Continue.equals("y")) {
    			continue;
    		}else {
    			System.out.println("Bye bye :)");
    			break;
    		}
    			
    	}
    	
    }
    public String checkwinner(String comcho, String placho) {
    	String winner = "draw";
    	if(comcho.equals("rock") && placho.equals("scissors")) {
    		winner = "Computer";
    		computerScore +=1;
    		roundCounter +=1;
    	}else if(comcho.equals("scissors") && placho.equals("rock")) {
    		winner = "Human";
    		humanScore +=1;
    		roundCounter +=1;
    	}else if(comcho.equals("paper") && placho.equals("scissors")){
    		winner = "Human";
    		humanScore +=1;
    		roundCounter +=1;
    	}else if(comcho.equals("scissors") && placho.equals("paper")){
    		winner = "Computer";
    		computerScore +=1;
    		roundCounter +=1;
    	}else if(comcho.equals("paper") && placho.equals("rock")){
    		winner = "Computer";
    		computerScore +=1;
    		roundCounter +=1;
    	}else if(comcho.equals("rock") && placho.equals("paper")) {
    		winner = "Human";
    		humanScore +=1;
    		roundCounter +=1;
    	}else {
    		roundCounter +=1;
    	}
    	
    	return winner;
    }
    
    
    
    public String validate(String prompt) {
    	
    	String input = readInput(prompt);
    	while(true) {
    		for(String choices : rpsChoices) {
    			if (!choices.equals(input)){
    				continue;
    			}else {
    				return input;
    			}
    		}
    		input = readInput("I do not understand "+input+". Could you try again?");
    	}
    	
		
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
